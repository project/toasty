<?php

declare(strict_types = 1);

namespace Drupal\toasty\Messenger\Middleware;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Link;
use Drupal\stamps\Messenger\Stamp\ActionsStamp;
use Drupal\stamps\Messenger\Stamp\CurrentUserStamp;
use Drupal\stamps\Messenger\Stamp\DescriptionStamp;
use Drupal\stamps\Messenger\Stamp\LinksStamp;
use Drupal\toasty\BusLog;
use Pusher\PusherInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Middleware\MiddlewareInterface;
use Symfony\Component\Messenger\Middleware\StackInterface;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use Symfony\Component\Messenger\Stamp\ReceivedStamp;

/**
 * Post handle.
 *
 * Sends notifications after messages have been handled.
 *
 * We use an ordered-last middleware instead of event as event only works with
 * transports.
 */
final class ToastyPostHandleMiddleware implements MiddlewareInterface {

  /**
   * Creates a new ToastyPostHandleMiddleware.
   */
  public function __construct(
    private PusherInterface $pusher,
    private TimeInterface $time,
  )
  {
  }

  /**
   * {@inheritdoc}
   */
  public function handle(Envelope $envelope, StackInterface $stack): Envelope {
    if (null === $envelope->last(HandledStamp::class)) {
      throw new \LogicException('This shouldnt happen as HandleStamp is required.');
    }

    $this->handleEnvelope($envelope);

    return $stack->next()->handle($envelope, $stack);
  }

  /**
   * {@inheritdoc}
   */
  private function handleEnvelope(Envelope $envelope): void {
    $currentUserStamp = $envelope->last(CurrentUserStamp::class);
    if ($currentUserStamp === NULL) {
      // Ignore messages without a user target.
      return;
    }

    if ($currentUserStamp->getUserId() === 0) {
      // Ignore anonymous.
      return;
    }

    $descriptionUserStamp = $envelope->last(DescriptionStamp::class);
    if ($descriptionUserStamp === NULL) {
      return;
    }

    $receiverStamp = $envelope->last(ReceivedStamp::class);
    $receiverName = $receiverStamp !== NULL ? $receiverStamp->getTransportName() : NULL;

    $className = $envelope->getMessage()::class;

    $log = (new BusLog($className))
      ->setCreated(new \DateTimeImmutable('@' . $this->time->getCurrentTime()))
      ->setReceiverName($receiverName)
      ->setClassName($className)
      ->setUserById($currentUserStamp->getUserId())
      ->setTitle($descriptionUserStamp->getTitle())
      ->setDescription($descriptionUserStamp->getDescription() ?? '');

    $linksStamp = $envelope->last(LinksStamp::class);
    if (NULL !== $linksStamp) {
      foreach ($linksStamp->getLinks() as $link) {
        $log->addLink((string) $link->getText(), $link->getUrl());
      }
    }

    $linksStamp = $envelope->last(ActionsStamp::class);
    if (NULL !== $linksStamp) {
      foreach ($linksStamp->getLinks() as [$link, $actionClass]) {
        // @todo do something with action class.
        $log->addLink((string) $link->getText(), $link->getUrl());
      }
    }

    $log2Data = static function (BusLog $log): array {
      $data = [];
      $data['title'] = $log->getTitle();
      $data['description'] = $log->getDescription();
      $data['date'] = $log->getCreated()->getTimestamp();
      $data['links'] = array_map(
        fn (Link $link) => [
          'url' => $link->getUrl()->toString(),
          'title' => $link->getText(),
        ],
        $log->getLinks(),
      );

      return $data;
    };

    $data = $log2Data($log);

    // sendToUser() isn't on the Pusher object yet.
    // See https://github.com/pusher/pusher-http-php/pull/379/
    $this->pusher->sendToUser((string) $currentUserStamp->getUserId(), 'bus-logs', $data);
  }

}

<?php

declare(strict_types=1);

namespace Drupal\toasty;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;

/**
 * Service provider for Toasty.
 */
final class ToastyServiceProvider implements ServiceProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $container
      // After MessengerPass.
      ->addCompilerPass(new ToastyPostCompilerPass(), priority: -100);
  }

}

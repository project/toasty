<?php

declare(strict_types=1);

namespace Drupal\toasty;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Bus post MessengerPass compiler pass.
 */
final class ToastyPostCompilerPass implements CompilerPassInterface {

  /**
   * {@inheritdoc}
   */
  public function process(ContainerBuilder $container): void {
    $buses = array_keys($container->findTaggedServiceIds('messenger.bus'));
    foreach ($buses as $bus) {
      $definition = $container->getDefinition($bus);
      /** @var \Symfony\Component\DependencyInjection\Argument\IteratorArgument $arg0 */
      $arg0 = $definition->getArgument(0);
      $values = $arg0->getValues();
      // @todo ensure this is after HandleMessageMiddleware
      $values[] = new Reference('messenger.middleware.toasty_post_handle');
      // @todo do the sub definition thing where the middleware is different see MessengerPass
      $arg0->setValues($values);
    }
  }

}

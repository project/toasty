<?php

declare(strict_types=1);

namespace Drupal\toasty;

use Drupal\Core\Link;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\user\UserInterface;

/**
 * @internal This class is subject to change/removal at any time.
 */
final class BusLog implements BusLogInterface {

  private ?string $receiverName;

  /**
   * @var class-string
   */
  private string $className;

  private string $title;

  /**
   * @var array|mixed[]
   */
  private array $titleVariables;

  private string $description;

  /**
   * @var array|mixed[]
   */
  private array $descriptionVariables;

  private ?\DateTimeInterface $created;

  /**
   * @var positive-int
   */
  private int $userId;

  private array $links = [];

  public function __construct(string $className) {
    $this->className = $className;
  }

  public function setReceiverName(?string $receiverName)
  {
    $this->receiverName = $receiverName;

    return $this;
  }

  public function getReceiverName(): ?string
  {
    return $this->receiverName;
  }

  public function setClassName(string $className)
  {
    $this->className = $className;

    return $this;
  }

  public function getClassName(): string
  {
    return $this->className;
  }


  public function getTitle(): TranslatableMarkup
  {
    return new TranslatableMarkup($this->title, $this->titleVariables);
  }

  public function getDescription(): TranslatableMarkup
  {
    return new TranslatableMarkup($this->description, $this->descriptionVariables);
  }

  public function setTitle(TranslatableMarkup|string $title) {
    $this->title = $title instanceof TranslatableMarkup ? $title->getUntranslatedString() : $title;
    $this->titleVariables = $title instanceof TranslatableMarkup ? $title->getArguments() : [];
    return $this;
  }

  public function setDescription(TranslatableMarkup|string $description) {
    $this->description = $description instanceof TranslatableMarkup ? $description->getUntranslatedString() : $description;
    $this->descriptionVariables = $description instanceof TranslatableMarkup ? $description->getArguments() : [];
    return $this;
  }

  public function getCreated(): \DateTimeInterface {
    return $this->created ?? throw new \Exception('Missing date');
  }

  public function setCreated(\DateTimeInterface $dateTime) {
    $this->created = $dateTime;
    return $this;
  }

  public function setUser(UserInterface $user) {
    $this->userId = (int) $user->id();
    return $this;
  }

  public function setUserById(int $uid) {
    $this->userId = $uid;
    return $this;
  }

  public function getUserId(): int
  {
    return $this->userId;
  }

  public function addLink(string $title, Url $url) {
    $this->links[] = [
      'title' => $title,
      'uri' => $url->toUriString(),
    ];
    return $this;
  }

  public function getLinks()
  {
    return array_map(function (array $item): Link {
      ['title' => $title, 'uri' => $uri] = $item;
      return Link::fromTextAndUrl($title, Url::fromUri($uri));
    }, $this->links);
  }

}

<?php

declare(strict_types = 1);

namespace Drupal\toasty;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\user\UserInterface;

/**
 * @internal This interface is subject to change/removal at any time.
 */
interface BusLogInterface {

  /**
   * @param class-string $className
   */
  public function __construct(string $className);

  /**
   * @return $this
   */
  public function setTitle(TranslatableMarkup|string $title);

  /**
   * @return \Drupal\Core\Link[]
   */
  public function getLinks();

  /**
   * @param class-string $className
   *
   * @return $this
   */
  public function setClassName(string $className);

  /**
   * @return $this
   */
  public function setCreated(\DateTimeInterface $dateTime);

  public function getCreated(): \DateTimeInterface;

  /**
   * @phpstan-param positive-int $uid
   *
   * @return $this
   */
  public function setUserById(int $uid);

  public function getReceiverName(): ?string;

  /**
   * @return $this
   */
  public function addLink(string $title, Url $url);

  /**
   * @return $this
   */
  public function setUser(UserInterface $user);

  public function getTitle(): TranslatableMarkup;

  /**
   * @return $this
   */
  public function setDescription(TranslatableMarkup|string $description);

  /**
   * @return $this
   */
  public function setReceiverName(?string $receiverName);

  public function getDescription(): TranslatableMarkup;

  /**
   * @return positive-int
   */
  public function getUserId(): int;

  /**
   * @return class-string
   */
  public function getClassName(): string;

}

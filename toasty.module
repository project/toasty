<?php

declare(strict_types = 1);

use Drupal\Core\Cache\CacheableMetadata;

/**
 * Implements hook_page_bottom().
 */
function toasty_page_bottom(array &$page_bottom): void {
  $page_bottom['toasty'] = [];
  (new CacheableMetadata())
    ->addCacheContexts(['user.permissions'])
    ->applyTo($page_bottom['toasty']);

  if (\Drupal::currentUser()->isAuthenticated()) {
    $page_bottom['toasty']['toaster'] = [
      '#type' => 'html_tag',
      '#tag' => 'div',
      '#attributes' => [
        'class' => ['toasty-toaster'],
      ],
    ];
    $page_bottom['toasty']['toaster']['#attached']['library'][] = 'toasty/toaster';
  }
}
